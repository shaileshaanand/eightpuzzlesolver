import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
public class EightPuzzleSolver {
    public EightPuzzleState aStarSearch(EightPuzzleState initialState) {
        PriorityQueue<EightPuzzleState> frontier=new PriorityQueue<>();
        ArrayList<EightPuzzleState> explored=new ArrayList<>();
        EightPuzzleState currentState;
        int exploredNodes=0;
        frontier.add(initialState);
        while (frontier.size()!=0) {
            currentState=frontier.poll();
            explored.add(currentState);
            exploredNodes++;
            System.out.print(exploredNodes+" nodes explored                  \r");
            if (currentState.isGoalState()) {
                System.out.println(exploredNodes+" nodes explored");
                return currentState;
            }
            ArrayList<EightPuzzleState> neighbours=currentState.getNeighbours(); 
            for (EightPuzzleState neighbouri : neighbours){
                if (!(frontier.contains(neighbouri) || explored.contains(neighbouri))) {
                    frontier.add(neighbouri);
                }
            }
        }
        return null;
    }
    public static void main(String[] args) {
        EightPuzzleState init=new EightPuzzleState(args[0]);
        EightPuzzleSolver a=new EightPuzzleSolver();
        StringBuilder result=new StringBuilder("}");
        int searchDepth=0;
        EightPuzzleState goal=a.aStarSearch(init);
        if (goal!=null) {
            while (goal.getParent()!=null && goal.getCausingAction()!='n') {
                result.append(goal.getCausingAction());
                result.append(',');
                searchDepth++;
                goal=goal.getParent();
            }
        }
        System.out.println("Search Depth="+searchDepth);
        result.append("{");
        result.reverse();
        result.delete(1,2);
        System.out.println(result);
    }
}
            




