import java.util.Arrays;
import java.util.ArrayList;
public class EightPuzzleState implements Comparable<EightPuzzleState> {
    private final char[] arrangement;
    private final char causingAction;
    private EightPuzzleState parent;
    private final int depth;
    private final int aStarScore;
    public EightPuzzleState(String arrangement,char causingAction,EightPuzzleState parent1,int depth) {
        this.arrangement=arrangement.toCharArray();
        this.causingAction=causingAction;
        if (parent1!=null) {
            parent=new EightPuzzleState(parent1);
        } else {
            parent=null;
        }
        this.depth=depth;
        this.aStarScore=calculateAStarScore();
    }
    public EightPuzzleState (String arrangement) {
        this(arrangement,'n',null,0);
    }
    public EightPuzzleState(EightPuzzleState o) {
        this.arrangement=o.arrangement;
        this.causingAction=o.causingAction;
        if (o.parent!=null) {
            this.parent=o.parent;
        } else {
            this.parent=null;
        }
        this.depth=o.depth;
        this.aStarScore=o.aStarScore;
    }
    public char getCausingAction() {
        return this.causingAction;
    }
    public EightPuzzleState getParent() {
        if (this.parent!=null) {
            return new EightPuzzleState(parent);
        } else {
            return null;
        }
    }
    public int getDepth() {
        return depth;
    }
    public char[] getArrangement() {
        return arrangement;
    }
    public boolean isGoalState() {
        char[] goalArray={'1','2','3','4','5','6','7','8','0'};
        return Arrays.equals(arrangement,goalArray);
    }
    public ArrayList<EightPuzzleState> getNeighbours() {
        ArrayList<EightPuzzleState> neighbours=new ArrayList<>();
        char temp;
        int index=new String(arrangement).indexOf('0');
        if (index==0||index==1||index==2||index==3||index==4||index==5) {
            char[] upNeighbour=Arrays.copyOf(arrangement,arrangement.length);
            temp=upNeighbour[index];
            upNeighbour[index]=upNeighbour[index+3];
            upNeighbour[index+3]=temp;
            neighbours.add(new EightPuzzleState(new String(upNeighbour),'u',this,this.depth+1));
        }
        if (index==0||index==1||index==3||index==4||index==6||index==7) {
            char[] leftNeighbour=Arrays.copyOf(arrangement,arrangement.length);
            temp=leftNeighbour[index];
            leftNeighbour[index]=leftNeighbour[index+1];
            leftNeighbour[index+1]=temp;
            neighbours.add(new EightPuzzleState(new String(leftNeighbour),'l',this,this.depth+1));
        }
        if (index==3||index==4||index==5||index==6||index==7||index==8) {
            char[] downNeighbour=Arrays.copyOf(arrangement,arrangement.length);
            temp=downNeighbour[index];
            downNeighbour[index]=downNeighbour[index-3];
            downNeighbour[index-3]=temp;
            neighbours.add(new EightPuzzleState(new String(downNeighbour),'d',this,this.depth+1));
        }
        if (index==1||index==2||index==4||index==5||index==7||index==8) {
            char[] rightNeighbour=Arrays.copyOf(arrangement,arrangement.length);
            temp=rightNeighbour[index];
            rightNeighbour[index]=rightNeighbour[index-1];
            rightNeighbour[index-1]=temp;
            neighbours.add(new EightPuzzleState(new String(rightNeighbour),'r',this,this.depth+1));
        }
        return neighbours;
    }
    public int getAStarScore() {
        return aStarScore;
    }
    public int calculateAStarScore() {
        int i,difference,score=0,initialTile;
        for (i=0;i<9;i++) {
            if (Character.getNumericValue(arrangement[i])>0) {
                difference=Math.abs(Character.getNumericValue(arrangement[i])-i-1);
                initialTile=Math.min(Character.getNumericValue(arrangement[i]),i);
            } else {
                continue;
            }
            switch(difference) {
                case 1:
                if (initialTile==3 || initialTile==6) {
                    score+=3; 
                } else {
                score+=1;
                }
                break;
                case 2:
                score+=2;
                break;
                case 3:
                score+=1;
                break;
                case 4:
                if (initialTile==3) {
                    score+=4;
                } else {
                    score+=2;
                }
                break;
                case 5:
                score+=3;
                break;
                case 6:
                score+=2;
                break;
                case 7:
                score+=3;
                break;
                case 8:
                score+=4;
                break;
            }
        }
        score+=this.depth;
        return score;
    }
    @Override
    public int compareTo(EightPuzzleState o) {
        return (this.getAStarScore()-o.getAStarScore());
    }
    @Override
    public int hashCode() {
        return new String(arrangement).hashCode();
    }
    @Override
    public boolean equals(Object o) {
        EightPuzzleState obj=(EightPuzzleState)o;
        if (this==o) {
            return true;
        } else if (Arrays.equals(this.arrangement,obj.arrangement)) {
            return true;
        }
        return false;
    }
}

