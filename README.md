## 8-Puzzle Solver

8-Puzzle Solver is java program to solve the 8-Puzzle using the A* Algorithm.
The 8-Puzzle game can be played [here](http://www.puzzlopia.com/puzzles/puzzle-8/play). On Android it can be played on the [8 Puzzle](https://play.google.com/store/apps/details?id=com.belmikri.eightpuzzle) app.

## Motivation

While taking [this](https://courses.edx.org/courses/course-v1:ColumbiaX+CSMM.101x+3T2018/course/) course on [Edx.org](edx.org), I came across this problem at the end of week 2.
 
## Screenshots

![screenshot](https://imgur.com/dIgSQTs.jpg)


## Features
 - Can solve any 8-Puzzle Problem in atmost 10000 checks
 - supports 2 goal states

## Build Instructions

 - git and jdk 8 should be installed
 - `git clone https://gitlab.com/shaileshaanand/eightpuzzlesolver.git`
 - `cd eightpuzzlesolver`
 - `javac *.java`
 - `java EightPuzzleSolver 527631840` (replace `527631840` with your own 8 puzzle problem)

## Problem formatting and output format

The problem can be formatted as a string of 9 digits with column 1 followed by column 2 and so on.

For example

5 2 7  
6 3 1  
8 4 0  
becomes `527631840`  

The output format is {l,r,u,l,.......}  
here,
 - l means left  
 - r means right  
 - u means up  
 - d means down  

So,  
After `d`  
5 2 7  
6 3 1  
8 4 0  
becomes  
5 2 7  
6 3 0  
8 4 1  
which is equivalent to `527630841` 


## License
GPL v3

© [Shailesh Aanand](aanand.cf)
